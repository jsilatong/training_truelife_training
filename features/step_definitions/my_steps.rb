# encoding: utf-8

Given(/^two numbers "(.*?)" and "(.*?)"$/) do |arg1, arg2|
  @num1 = arg1.to_i
  @num2 = arg2.to_i
end

When(/^I add both together$/) do
  @result = @num1 + @num2
end

Then(/^I should get "(.*?)"$/) do |arg1|
  raise unless @result == arg1.to_i
end
