Feature: Starting cucumber and running
	In order to learn BDD
	As a developer
	I want to run cucumber

@wip
Scenario: A simple calculation

	Given two numbers "1" and "2"
	When I add both together
	Then I should get "3"

@demo
Scenario Outline: Sign in to system
	Given I signed in with "<username>" and "<password>"

	Examples:
	|username | password |
	| user1	  | pass1    |
	| user2	  | pass2    |
	| user3	  | pass3    |

